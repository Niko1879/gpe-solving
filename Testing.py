import numpy as np
import math as mth
import matplotlib.pyplot as plt

###############################################################################
##############################2D VERSION#######################################
###############################################################################

#Try changing mu in each version and see what happens to f (or psi)!

def F(x):
   return np.sin(x) + np.cos(x)
    
#Right hand side of the equation
def G(x, f, h, mu):
    #estimate derivatives using finite differences
    fprime = np.concatenate(([(f[1] - f[0])/h], (f[2:] - f[:-2])/(2*h), 
                             [(f[-1] - f[-2])/h]))
    
    fdprime = np.concatenate(([(fprime[1] - fprime[0])/h], 
                               (fprime[2:] - fprime[:-2])/(2*h), 
                              [(fprime[-1] - fprime[-2])/h]))
    
    return fdprime - (np.absolute(f)**2) * f + mu*f

def solve_dp(domain, h, tolerance):
    #discretize domain
    n = int((domain[-1] - domain[0])/h +1)
    x = np.zeros(n)
    for i in range(n):
        x[i] += i*h
    #discretize initial guess
    f = F(x)
    dt = h/200
    temp = 0
    while True:
        temp += 1
        f_prev = np.copy(f)
        mu = 4
        #compute k1-4
        k_1 = G(x, f, h, mu)
        k_2 = G(x, f + k_1*dt/2, h, mu)
        k_3 = G(x, f + k_2*dt/2, h, mu)
        k_4 = G(x, f + k_3*dt, h, mu)
        f = f + (dt/6)*(k_1 + 2*k_2 + 2*k_3 + k_4)
        if np.linalg.norm(f - f_prev) < tolerance:
            break
        #If the solution diverges for some reason
        #if np.linalg.norm(f - f_prev) > 10000:
        #    break
        #print(h*sum(np.absolute(f)**2))
        
    return f, x
    
#f, x = solve_dp([0, 10], 0.01, 0.000001)
#print(f**2) 
#X = np.linspace(0, 20,endpoint=True)
#plt.plot(x, f)
    
###############################################################################
##############################3D VERSION#######################################
###############################################################################

def init_con(x, y):
    return x**3 + y**3

def G2(x, y, psi, h_x, h_y, mu):
    psi_x = np.concatenate(((psi[:,1,None] - psi[:,0,None])/h_x, (psi[:,2:] - 
                             psi[:,:-2])/(2*h_x), (psi[:,-1,None] - 
                             psi[:,-2,None])/h_x), axis=1)

    psi_xx = np.concatenate(((psi_x[:,1,None] - psi_x[:,0,None])/h_x, 
                             (psi_x[:,2:] - psi_x[:,:-2])/(2*h_x), 
                             (psi_x[:,-1,None] - psi_x[:,-2,None])/h_x), 
                             axis=1)
    
    psi_y = np.concatenate(((psi[1,None] - psi[0,None])/h_y, (psi[2:] - 
                             psi[:-2])/(2*h_y), (psi[-1,None] - 
                             psi[-2,None])/h_y), axis=0)

    psi_yy = np.concatenate(((psi_y[1,None] - psi_y[0,None])/h_y, (psi_y[2:] - 
                              psi_y[:-2])/(2*h_y), (psi_y[-1,None] - 
                              psi_y[-2,None])/h_y), axis=0)
    
    return (psi_xx + psi_yy) - (np.absolute(psi)**2)*psi + mu*psi

#NOTE: VERY small dt value is required for larger domains for some reason
def solve_2d(domain_x, domain_y, h_x, h_y, mu, tolerance):
    x_min = domain_x[0]
    x_max = domain_x[-1]
    y_min = domain_y[0]
    y_max = domain_y[-1]
    n = int((x_max - x_min)/h_x +1) #dim y
    m = int((y_max - y_min)/h_y +1) #dim x
    x = np.zeros((m, 1))
    y = np.zeros((n, 1))
    #discretize x
    index = 0
    itr = x_min
    while (index < m):
        x[index] = itr
        itr += h_x
        index += 1
    #discretize y    
    index = 0
    itr = y_min    
    while (index < n):
        y[index] = itr
        itr += h_y
        index += 1    
    
    x, y = np.meshgrid(x, y)
    psi = init_con(x, y)
    dt = (1/1000)*((h_x + h_y)/2)
    timeout = 0
    while True:
        psi_prev = np.copy(psi)
        k_1 = G2(x, y, psi, h_x, h_y, mu)
        k_2 = G2(x, y, psi + k_1*dt/2, h_x, h_y, mu)
        k_3 = G2(x, y, psi + k_2*dt/2, h_x, h_y, mu)
        k_4 = G2(x, y, psi + k_3*dt, h_x, h_y, mu)
        
        psi = psi + (dt/6)*(k_1 + 2*k_2 + 2*k_3 + k_4)
        if np.linalg.norm(psi - psi_prev) < tolerance:
            break
        if np.linalg.norm(psi) - np.linalg.norm(psi_prev) > 10000000:
            break
        timeout += 1
        if (timeout > 10000000):
            break
        
    return psi, x, y

psi, x, y = solve_2d([0, 4], [0, 4], 0.1, 0.1, 1, 0.00000000001)
print(psi)
    
#X = np.random.random((3, 3))+1
#print(X)
#print("SPACE")
#print(X[:-2])