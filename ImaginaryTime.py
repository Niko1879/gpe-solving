# -*- coding: utf-8 -*-
"""
Created on Sat Feb  2 00:17:42 2019

@author: nik_7
"""

import numpy as np
import math as mth
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
np.set_printoptions(threshold=np.nan)
def F(eta):
    eta_max = eta[-1]
    return (eta/eta_max) + 0.2*(1 - np.cos(2*mth.pi*eta/(eta_max)))

def solve_1d(domain, h, s, tolerance):
    #discretize domain
    n = int((domain[-1] - domain[0])/h +1)
    eta = np.zeros(n)
    for i in range(n):
        eta[i] += i*h
    #discretize initial guess
    eta_max = eta[n-1]
    f = F(eta)
    dt = h/20
    N_0 = 200
    #loop should start here?
    
    #approximate first and second derivative of f using finite differences
    #CHANGE THESE, this method is slow
    
   # fprime = np.zeros(n)
   # fprime[0] = (f[1] - f[0])/h
   # fprime[n-1] = (f[n-1] - f[n-2])/h
   # for i in range(1, n-1):
   #     fprime[i] = (f[i+1] - f[i-1])/(2*h)
   # 
   # fdprime = np.zeros(n)
   # fdprime[0] = (f[2] -2*f[1] + f[0])/(h**2)
   # fdprime[n-1] = (f[n-1] - 2*f[n-2] + f[n-3])/(h**2)
   # for i in range(1, n-1):
   #     fdprime[i] = (f[i+1] + f[i-1] - 2*f[i])/(h**2)
   # print(fdprime)
    
    while True:
        #estimate derivatives using finite differences
        fprime = np.concatenate(([(f[1] - f[0])/h], (f[2:] - f[:-2])/(2*h), [(f[n-1] - f[n-2])/h])) 
        fdprime = np.concatenate(([(f[2] - 2*f[1] + f[0])/(h**2)], (f[2:] - 2*f[1:-1] + f[:-2])/(h**2), [(f[n-1] - 2*f[n-2] + f[n-3])/(h**2)]))
        #compute mu
        mu = (-fdprime[n-1] - (1/eta_max)*fprime[n-1] + (s**2)*f[n-1]/(eta_max**2) + f[n-1]**3)/f[n-1]
        f_temp = np.copy(f)
        #compute k1
        k_1 = np.concatenate(([0], (fdprime[1:-1] + fprime[1:-1]/eta[1:-1] - (s**2)*f[1:-1]/(eta[1:-1]**2) - f[1:-1]**3 + mu*f[1:-1])  ,[0]))
        
        #compute k2
        f = f_temp + k_1*dt/2
        k_2 = np.concatenate(([0], (fdprime[1:-1] + fprime[1:-1]/eta[1:-1] - (s**2)*f[1:-1]/(eta[1:-1]**2) - f[1:-1]**3 + mu*f[1:-1])  ,[0]))
        f = f_temp + k_2*dt/2
        k_3 = np.concatenate(([0], (fdprime[1:-1] + fprime[1:-1]/eta[1:-1] - (s**2)*f[1:-1]/(eta[1:-1]**2) - f[1:-1]**3 + mu*f[1:-1])  ,[0]))

        #compute k_4
        f = f_temp + k_3*dt
        k_4 = np.concatenate(([0], (fdprime[1:-1] + fprime[1:-1]/eta[1:-1] - (s**2)*f[1:-1]/(eta[1:-1]**2) - f[1:-1]**3 + mu*f[1:-1])  ,[0]))
        
        f = f_temp + (dt/6)*(k_1 + 2*k_2 + 2*k_3 + k_4)
        #renormalize mass (not sure how this works, just copied Ben for now until I can ask for explanation)
        S_1 = sum(eta*((f)**2))
        N_1=h*S_1
        f=(f*mth.sqrt(N_0))/(mth.sqrt(N_1))
        if np.linalg.norm(f - f_temp) < tolerance:
            break
        if np.linalg.norm(f - f_temp) > 10000:
            break
        
    return f
    
#f = solve_1d([0, 20], 0.1, 1, 0.00001)    
X = np.linspace(0, 20,endpoint=True)
#plt.plot(f)

def temp_func(x, y):
    return np.cos(x) + np.sin(y)*1j
    #return np.ones(x.shape);

def ground_state(x, y):
    return np.exp(1j * np.arctan(np.real(y)/x))

def solve_2d(domain_x, domain_y, h_x, h_y, s, tolerance):
    x_min = domain_x[0]
    x_max = domain_x[-1]
    y_min = domain_y[0]
    y_max = domain_y[-1]
    n = int((x_max - x_min)/h_x +1) #dim y
    m = int((y_max - y_min)/h_y +1) #dim x
    x = np.zeros((m, 1))
    y = np.zeros((n, 1))
    #discretize x
    index = 0
    itr = x_min
    while (index < m):
        x[index] = itr
        itr += h_x
        index += 1
    #discretize y    
    index = 0
    itr = y_min    
    while (index < n):
        y[index] = itr
        itr += h_y
        index += 1    
    
    x, y = np.meshgrid(x, y)
    psi = temp_func(x, y)
    mass = np.sum(np.absolute(psi)**2)*h_x*h_y
    #psi = (1/np.sqrt(mass))*psi
    dt = (1/200)*(h_x + h_y)
    tmp = 0
   
    while True:
        psi_xx = (1/(h_x**2))*np.concatenate(((psi[:,2,None] - 2*psi[:,1,None] + psi[:,0,None]), (psi[:,2:] - 2*psi[:,1:-1] + psi[:,:-2]), psi[:,-1,None] - 2*psi[:,-2,None] + psi[:,-3,None]), axis=1)
        psi_yy = (1/(h_y**2))*np.concatenate(((psi[2,None] - 2*psi[1,None] + psi[0,None]), (psi[2:] - 2*psi[1:-1] + psi[:-2]), psi[-1,None] - 2*psi[-2,None] + psi[-3,None]), axis=0)
        psi_temp = np.copy(psi)
        #mu = (-(psi_xx[m-1][n-1] + psi_yy[m-1][n-1]) + ((np.absolute(psi)[m-1][n-1])**2)*psi[m-1][n-1])/psi[m-1][n-1]
        mu = 1
        k_1 =  (psi_xx + psi_yy) - (np.absolute(psi)**2)*psi + mu*psi
        psi = psi_temp + k_1*dt/2
        psi_xx = (1/(h_x**2))*np.concatenate(((psi[:,2,None] - 2*psi[:,1,None] + psi[:,0,None]), (psi[:,2:] - 2*psi[:,1:-1] + psi[:,:-2]), psi[:,-1,None] - 2*psi[:,-2,None] + psi[:,-3,None]), axis=1)
        psi_yy = (1/(h_y**2))*np.concatenate(((psi[2,None] - 2*psi[1,None] + psi[0,None]), (psi[2:] - 2*psi[1:-1] + psi[:-2]), psi[-1,None] - 2*psi[-2,None] + psi[-3,None]), axis=0)
        k_2 =  (psi_xx + psi_yy) - (np.absolute(psi)**2)*psi + mu*psi
        psi = psi_temp + k_2*dt/2
        psi_xx = (1/(h_x**2))*np.concatenate(((psi[:,2,None] - 2*psi[:,1,None] + psi[:,0,None]), (psi[:,2:] - 2*psi[:,1:-1] + psi[:,:-2]), psi[:,-1,None] - 2*psi[:,-2,None] + psi[:,-3,None]), axis=1)
        psi_yy = (1/(h_y**2))*np.concatenate(((psi[2,None] - 2*psi[1,None] + psi[0,None]), (psi[2:] - 2*psi[1:-1] + psi[:-2]), psi[-1,None] - 2*psi[-2,None] + psi[-3,None]), axis=0)
        k_3 = (psi_xx + psi_yy) - (np.absolute(psi)**2)*psi + mu*psi
        psi = psi_temp + k_3*dt
        psi_xx = (1/(h_x**2))*np.concatenate(((psi[:,2,None] - 2*psi[:,1,None] + psi[:,0,None]), (psi[:,2:] - 2*psi[:,1:-1] + psi[:,:-2]), psi[:,-1,None] - 2*psi[:,-2,None] + psi[:,-3,None]), axis=1)
        psi_yy = (1/(h_y**2))*np.concatenate(((psi[2,None] - 2*psi[1,None] + psi[0,None]), (psi[2:] - 2*psi[1:-1] + psi[:-2]), psi[-1,None] - 2*psi[-2,None] + psi[-3,None]), axis=0)
        k_4 =  (psi_xx + psi_yy) - (np.absolute(psi)**2)*psi + mu*psi
        
        psi = psi_temp + (dt/6)*(k_1 + 2*k_2 + 2*k_3 + k_4)
        #if mod_psi_new - mod_psi < tolerance:
        #    break
        #if mod_psi_new - mod_psi > 100000:
        #    break
        #mass_new = np.sum(np.absolute(psi)**2)*h_x*h_y
        #psi = (1/np.sqrt(mass_new))*psi
        tmp += 1
        if (tmp > 1000):
            break
        
    return psi, x, y
psi, x, y = solve_2d([-10, 10],[-10, 10], 0.1, 0.1, 1, 0.0001)

fig = plt.figure()
ax = fig.gca(projection='3d')
Axes3D.plot_surface(ax, x, y, np.absolute(psi)**2)
plt.show()
    
